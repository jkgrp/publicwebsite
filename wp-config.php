<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'stagingl_lbkdsn');

/** MySQL database username */
define('DB_USER', 'stagingl_lbkdsn');

/** MySQL database password */
define('DB_PASSWORD', '-S3MPX6@W5');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '458efamjuwjaxs5fwfshc0p0maoqghnuhdutx4zrvsonj1tmprcmmo2slreh45i3');
define('SECURE_AUTH_KEY',  'aipieom3entdddheymk60ncrbmctz0uycesgnusg9vsxdtg4fgdmsmbkp4hekro5');
define('LOGGED_IN_KEY',    '741qk9w38jd1r3ddpumadm4rb9uxvosmstmtta1r0wvi9opnzrwwfogjzcctmgla');
define('NONCE_KEY',        'qoblvqow0v2abhndhixiolqcphsbondo3dgsncvpesdenuvjt5dwm7a8h0rpqibw');
define('AUTH_SALT',        'tpoi1icoqf72vgqtujstjjituuxxrbh6ptn44ee1nflxn9yaykjlux93iq3nrmlp');
define('SECURE_AUTH_SALT', 'ymmz5spgdxqbyrr5awrhazoboerperarckazlheznsm7jealelrztecgfck6y8gx');
define('LOGGED_IN_SALT',   'qrdd67q0lar8lgup1r1rkszo9go8nirzlmoaegza5u82q5xhrqqvmngmlunnnpyt');
define('NONCE_SALT',       'sdg8ol4mh6a58awq5yrtenlnjzve9lkcrof0ulfjwf1rodp0sjjztlibzhdodcov');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
